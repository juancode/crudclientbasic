﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CrudClient.Models
{
    public class Client
    {
        public int clientId { get; set; }
        public string name { get; set; }
        public string apPaterno { get; set; }
        public string apMaterno { get; set; }
        public string email { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha Nacimiento")]
        public Nullable<System.DateTime> birthdate { get; set; }
        public string gender { get; set; }
        public Nullable<int> active { get; set; }
        public Nullable<System.DateTime> createdAt { get; set; }
    }
}
