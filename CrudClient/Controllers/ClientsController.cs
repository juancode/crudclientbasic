﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrudClient.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CrudClient.Controllers
{
    public class ClientsController : Controller
    {
        // GET: Clients
        public ActionResult Index()
        {
            var webClient = new System.Net.WebClient();
            var url = "https://localhost:44340/api/clients";
            var res = webClient.DownloadString(url);
            List<Client> data= Newtonsoft.Json.JsonConvert.DeserializeObject<List<Client>>(res);
            return View(data);
        }

        // GET: Clients/Details/5
        public ActionResult Details(int id)
        {
            var webClient = new System.Net.WebClient();
            var url = "https://localhost:44340/api/clients/"+ id.ToString();
            var res = webClient.DownloadString(url);
            Client data = Newtonsoft.Json.JsonConvert.DeserializeObject<Client>(res);
            return View(data);
        }

        // GET: Clients/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Clients/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Client client)
        {
            try
            {
                client.active = 1;
                client.createdAt = DateTime.Now;
                var webClient = new System.Net.WebClient();
                var url = "https://localhost:44340/api/clients";
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(client);

                webClient.Headers.Add("Content-Type", "application/json");

                webClient.UploadString(url, json);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Clients/Edit/5
        public ActionResult Edit(int id)
        {
            var webClient = new System.Net.WebClient();
            var url = "https://localhost:44340/api/clients/" + id.ToString();
            var res = webClient.DownloadString(url);
            Client data = Newtonsoft.Json.JsonConvert.DeserializeObject<Client>(res);
            
            return View(data);
        }

        // POST: Clients/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Client client)
        {
            try
            {
                client.active = 1;
                client.createdAt = client.createdAt;
                var webClient = new System.Net.WebClient();
                var url = "https://localhost:44340/api/clients/" + id.ToString();
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(client);

                webClient.Headers.Add("Content-Type", "application/json");

                webClient.UploadString(url,"PUT", json);

                return RedirectToAction(nameof(Index));                
            }
            catch
            {
                return View();
            }
        }

        // GET: Clients/Delete/5
        public ActionResult Delete(int id)
        {
            var webClient = new System.Net.WebClient();
            var url = "https://localhost:44340/api/clients/" + id.ToString();
            var res = webClient.DownloadString(url);
            Client data = Newtonsoft.Json.JsonConvert.DeserializeObject<Client>(res);
            return View(data);
        }

        // POST: Clients/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                var webClient = new System.Net.WebClient();
                var url = "https://localhost:44340/api/clients/" + id.ToString();

                webClient.UploadData(url, "DELETE", null);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        
    }
}